@extends('components.master')

@section('content')

<div class="banner margin_bottom_70">
    <div class="container">
        <h1 class="title-font title-banner">Promo</h1>
        <ul class="breadcrumb des-font">
            <li><a href="{{url('/')}}">Beranda</a></li>
            <li class="active">Promo</li>
        </ul>
    </div>
</div>

<div class="container blog-page margin_bottom_70">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content-blog">
            <div class="row">
                @forelse($promo as $item)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 space_right_10 margin_bottom_70">
                    <a href="{{ url('/content-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}"
                        class="inline-block over-hidden">
                        <img src="{{ $item['IMAGE'] }}" class="img-responsive hover-zoom-out" alt=""></a>
                    <h2 class="title-font capital title-post"><a href="{{ url('/content-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}">{{$item['TITLE']}}</a></h2>
                    <p class="des-font day-post">{{$item['SUBTITLE']}}</p>
                </div>
                @empty
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p>No data</p>
                </div>
                @endforelse

            </div>
        </div>
    </div>
</div>

@endsection