<!DOCTYPE html>
<html>

<head>
    <title>CKL Looks Indonesia</title>
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Aep Saefulloh">
    <link rel="icon" href="{{asset('assets')}}/img/favicon.jpeg" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets')}}/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets')}}/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets')}}/css/themify-icons.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets')}}/css/zoa-font.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets')}}/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets')}}/css/font-family.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets')}}/css/slick.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets')}}/css/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets')}}/css/style-main.css?v=<?php echo rand(); ?>">
    <link rel="stylesheet" type="text/css" href="{{asset('assets')}}/css/responsive.css">
</head>

<body>
    <div class="pushmenu menu-home5">
        <div class="menu-push">
            <span class="close-left js-close"><i class="ti-close"></i></span>
            <div class="clearfix"></div>
            <form role="search" method="get" id="searchform" class="searchform title-font" action="/search">
                <div>
                    <label class="screen-reader-text" for="q"></label>
                    <input type="text" placeholder="Search for products" value="" name="q" id="q" autocomplete="off">
                    <input type="hidden" name="type" value="product">
                    <button type="submit" id="searchsubmit"><i class="ti-search"></i></button>
                </div>
            </form>
            <ul class="nav-home5 js-menubar clear-space menu-font">
                <li class="level1 active dropdown">
                    <a href="#" class="uppercase">Home</a>
                    <span class="icon-sub-menu"></span>
                    <div class="menu-level1 js-open-menu">
                        <ul class="level1">
                            <li class="level2">
                                <a href="#" class="capital">home set 1</a>
                                <ul class="menu-level-2 clear-space">
                                    <li class="level3 capital menu-child-font"><a href="Home1.html" title="">home page
                                            1</a></li>
                                    <li class="level3 capital menu-child-font"><a href="Home2.html" title="">home page
                                            2</a></li>
                                    <li class="level3 capital menu-child-font"><a href="Home3.html" title="">home page
                                            3</a></li>
                                    <li class="level3 capital menu-child-font"><a href="Home4.html" title="">home page
                                            4</a></li>
                                </ul>
                            </li>
                            <li class="level2">
                                <a href="#" class="capital">home set 2</a>
                                <ul class="menu-level-2 clear-space">
                                    <li class="level3 capital menu-child-font"><a href="Home5.html" title="">home page
                                            5</a></li>
                                    <li class="level3 capital menu-child-font"><a href="Home6.html" title="">home page
                                            6</a></li>
                                    <li class="level3 capital menu-child-font"><a href="Home7.html" title="">home page
                                            7</a></li>
                                    <li class="level3 capital menu-child-font"><a href="Home8.html" title="">home page 8
                                            comming soon</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="level1 active dropdown"><a href="#" class="uppercase">Shop</a>
                    <span class="icon-sub-menu"></span>
                    <div class="menu-level1 js-open-menu">
                        <ul class="level1">
                            <li class="level2">
                                <a href="#" class="capital">Shop page</a>
                                <ul class="menu-level-2 clear-space">
                                    <li class="level3 menu-child-font"><a href="Shop_right_sidebar.html">shop right
                                            sidebar</a></li>
                                    <li class="level3 menu-child-font"><a href="Shop_left_sidebar.html">shop left
                                            sidebar</a></li>
                                    <li class="level3 menu-child-font"><a href="Shop_no_sidebar.html">shop full
                                            width</a></li>
                                    <li class="level3 menu-child-font"><a href="#">shop page 4 - comming soon</a></li>
                                </ul>
                            </li>
                            <li class="level2">
                                <a href="#" class="capital">Single Product Type</a>
                                <ul class="menu-level-2 clear-space">
                                    <li class="level3 menu-child-font"><a href="Product_detail_1.html">product detail
                                            1</a></li>
                                    <li class="level3 menu-child-font"><a href="Product_detail_2.html">product detail
                                            2</a></li>
                                    <li class="level3 menu-child-font"><a href="#">product detail 3 - Comming soon</a>
                                    </li>
                                    <li class="level3 menu-child-font"><a href="#">product detail 4 - Comming soon</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </li>
                <li class="level1 active dropdown">
                    <a href="#" class="uppercase">Blog</a>
                    <span class="icon-sub-menu"></span>
                    <div class="menu-level1 js-open-menu">
                        <ul class="level1">
                            <li class="level2">
                                <a href="#" class="capital">blog page</a>
                                <ul class="menu-level-2 clear-space">
                                    <li class="level3 capital menu-child-font"><a href="Blog_right_sidebar.html">blog
                                            right sidebar</a></li>
                                    <li class="level3 capital menu-child-font"><a href="Blog_left_sidebar.html">blog
                                            left sidebar</a></li>
                                    <li class="level3 capital menu-child-font"><a href="Blog_no_sidebar.html">blog full
                                            width</a></li>
                                    <li class="level3 capital menu-child-font"><a href="#">blog page 4 - comming
                                            soon</a></li>
                                </ul>
                            </li>
                            <li class="level2">
                                <a href="#" class="capital">blog detail</a>
                                <ul class="menu-level-2 clear-space">
                                    <li class="level3 capital menu-child-font"><a
                                            href="Blog_detail_right_sidebar.html">blog
                                            detail right sidebar</a></li>
                                    <li class="level3 capital menu-child-font"><a
                                            href="Blog_detail_left_sidebar.html">blog
                                            detail left sidebar</a></li>
                                    <li class="level3 capital menu-child-font"><a
                                            href="Blog_detail_no_sidebar.html">blog
                                            detail full width</a></li>
                                    <li class="level3 capital menu-child-font"><a href="#">blog detail 4 - comming
                                            soon</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="level1 active dropdown">
                    <a href="#" class="uppercase">Pages</a>
                    <span class="icon-sub-menu"></span>
                    <div class="menu-level1 js-open-menu">
                        <ul class="level1">
                            <li class="level2">
                                <a href="#" class="capital">page set 1</a>
                                <ul class="menu-level-2 clear-space">
                                    <li class="level3 capital menu-child-font"><a href="About_page_1.html">about page
                                            1</a></li>
                                    <li class="level3 capital menu-child-font"><a href="About_page_2.html">about page
                                            2</a></li>
                                    <li class="level3 capital menu-child-font"><a href="FAQ.html">FAQs page</a></li>
                                    <li class="level3 capital menu-child-font"><a href="404.html">404 page</a></li>
                                </ul>
                            </li>
                            <li class="level2">
                                <a href="#" class="capital">page set 2</a>
                                <ul class="menu-level-2 clear-space">
                                    <li class="level3 capital menu-child-font"><a href="cart_page.html">cart page</a>
                                    </li>
                                    <li class="level3 capital menu-child-font"><a href="wishlist_page.html">wishlist
                                            page</a></li>
                                    <li class="level3 capital menu-child-font"><a href="login_page.html">login/register
                                            page</a></li>
                                    <li class="level3 capital menu-child-font"><a href="#">other page - comming soon</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="level1">
                    <a href="Contact.html" class="uppercase">contact</a>
                </li>
                <li>
                    <a href="#" class="inline-block uppercase"><i class="zoa-icon-user space_right_10"></i>login</a>
                </li>
            </ul>
        </div>
    </div>
    <header>
        <div class="container space_top_bot_55 delay03" id="menu-header">
            <div class="row flex">
                <div class="col-lg-1 col-md-2 col-sm-6 col-xs-5 logo-top-home1">
                    <a href="{{url('/')}}"><img src="{{asset('assets')}}/img/logoo.png" width="100" alt=""></a>
                </div>
                <div class="col-lg-8 col-md-7 hidden-sm hidden-xs menu-center-home1">
                    <ul class="nav navbar-nav menu-font menu-main">
                        <li class="relative">
                            <a href="{{url('/')}}" class="link-menu delay03 uppercase">Beranda</a>
                        </li>
                        <li class="relative">
                            <a href="{{url('product')}}" class="link-menu delay03 uppercase">Produk</a>
                        </li>
                        <li class="relative">
                            <a href="{{url('content')}}" class="link-menu delay03 uppercase">Artikel</a>
                        </li>
                        <li class="relative dropdown">
                            <a href="{{url('contact')}}" class="link-menu delay03 uppercase">Kontak</a>
                            <!-- <figure class="line absolute delay03"></figure>
                            <div
                                class="dropdown-menu mega-menu-main absolute space_30 space_top_bot_50 text-left mega-menu-shop">
                                <div class="container_15">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 border-right">
                                            <ul class="clear-space capital lv1">
                                                <li class="margin_bottom_20 space_left_20 uppercase">Top Collection</li>
                                                <li class="menu-child-font"><a href="">Fashion
                                                        Kids</a></li>
                                                <li class="menu-child-font"><a href="">Sandal
                                                        Premium</a></li>
                                                <li class="menu-child-font"><a href="">Cassandra</a>
                                                </li>
                                                <li class="menu-child-font"><a href="#">Pajamas Pria</a></li>
                                                <li class="menu-child-font"><a href="#">Casual Style</a></li>
                                                <li class="menu-child-font"><a href="#">Exe Set</a></li>
                                                <li class="menu-child-font"><a href="#">Batch 14</a></li>

                                            </ul>
                                        </div>



                                        <div class="col-lg-4 col-md-4 banner-menu">
                                            <div class="product">
                                                <div class="img-product relative">
                                                    <a href="#" class="inline-block"><img
                                                            src="{{asset('assets')}}/img/example/ex-7.jpeg"
                                                            class="img-responsive" alt=""></a>
                                                    <figure class="absolute uppercase label-new title-font text-center">
                                                        new</figure>
                                                    <div class="product-icon absolute text-center">
                                                        <form method="post" action="/cart/add"
                                                            enctype="multipart/form-data"
                                                            class="inline-block icon-addcart">
                                                            <input type="hidden" name="id" value="" />
                                                            <button type="submit" name="add"
                                                                class="enj-add-to-cart-btn btn-default"><i
                                                                    class="ti-bag"></i></button>
                                                        </form>
                                                        <a href="#" class="icon-heart inline-block"><i
                                                                class="ti-heart"></i></a>
                                                        <a href="#"
                                                            class="engoj_btn_quickview icon-quickview inline-block"
                                                            title="quickview">
                                                            <i class="ti-more-alt"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </li>
                        <li class="relative">
                            <a href="{{route('promo')}}" class="link-menu delay03 uppercase">Promo</a>
                            <figure class="line absolute delay03"></figure>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-7 text-right icon-main">
                    <a href="#" class="link-menu delay03 container_20 inline-block hidden-xs hidden-sm"
                        id="btn-search"><i class="ti-search"></i></a>
                    <a href="#" class="link-menu delay03 container_20 inline-block hidden-xs hidden-sm"
                        id="btn-login"><i class="zoa-icon-user"></i></a>
                    <a href="{{ url('/cart') }}"
                        class="link-menu delay03 container_20 relative inline-block text-center" id="btn-cart">
                        <i class="ti-bag"></i>

                        <figure class="absolute label-cart number-font">
                            @if (isset($_SESSION['CART']) && isset($_SESSION['CART']['COUNT_QTY']) &&
                            $_SESSION['CART']['COUNT_QTY'] != null) {{ $_SESSION['CART']['COUNT_QTY'] }} @else 0 @endif
                        </figure>
                    </a>
                    <a href="#" class="link-menu delay03 inline-block hidden-md hidden-lg space_left_10 btn-push-menu">
                        <svg width="26" height="16" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                            x="0px" y="0px" viewBox="0 0 66 41" xmlns:xlink="http://www.w3.org/1999/xlink"
                            xml:space="preserve">
                            <g>
                                <line class="st0" x1="1.5" y1="1.5" x2="64.5" y2="1.5"></line>
                                <line class="st0" x1="1.5" y1="20.5" x2="64.5" y2="20.5"></line>
                                <line class="st0" x1="1.5" y1="39.5" x2="64.5" y2="39.5"></line>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </header>
    <main>
        @yield('content')
    </main>
    <footer>
        <div class="contact-home1 space_top_100 space_bot_120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-about-home1">
                        <h4 class="des-font space_bot_40 title-about-home1">CKL Looks</h4>
                        <p class="des-font space_bot_20 des-about-home1">Bandung, Indonesia. Since 2017.</p>
                        <!-- <p class="space_bot_30 location"><span class="ti-location-pin"></span><span> 531 West Avenue,
                            NY</span></p> -->
                        <div class="space_bot_50">
                            <a href="#"><img src="{{asset('assets')}}/img/img-footer-1.png" class="img-responsive"
                                    alt=""></a>
                        </div>

                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 lastest-post">
                        <h4 class="des-font space_bot_40 title-about-home1 text-left">Sitemap</h4>
                        <h5 class="title-font uppercase text-left"><a href="#">Home</a></h5>
                        <h5 class="title-font uppercase text-left"><a href="#">Product</a></h5>
                        <h5 class="title-font uppercase text-left"><a href="#">Article</a></h5>
                        <h5 class="title-font uppercase text-left"><a href="#">Contact</a></h5>
                        <h5 class="title-font uppercase text-left"><a href="#">Promo</a></h5>

                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 recent-tag">
                        <h4 class="des-font space_bot_40 title-about-home1">Find Us</h4>
                        <h5 class="title-font uppercase text-left"><a href="#">Whatsapp</a></h5>
                        <h5 class="title-font uppercase text-left"><a href="#">Playstore</a></h5>
                        <h5 class="title-font uppercase text-left"><a href="#">Tiktok Shop</a></h5>
                    </div>
                    <div class="col-lg-3 col-md-12 col-sm-6 col-xs-12 instagram">
                        <h4 class="des-font space_bot_40 title-about-home1">Instagram</h4>


                        <a href="#"><img src="{{asset('assets')}}/img/img_insta_1.jpg" class="img-responsive"
                                alt=""></a>


                        <a href="#"><img src="{{asset('assets')}}/img/img_insta_2.jpg" class="img-responsive"
                                alt=""></a>


                        <a href="#"><img src="{{asset('assets')}}/img/img_insta_3.jpg" class="img-responsive"
                                alt=""></a>


                        <a href="#"><img src="{{asset('assets')}}/img/img_insta_4.jpg" class="img-responsive"
                                alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid des-font text-center space_top_bot_50 copyright">Copyright © 2022 PT. CKL BRAND
            INDONESIA. All rights reserved.</div>

    </footer>
    <div class="overlay"></div>
    <div class="gotop text-center fade"><i class="ti-angle-up"></i></div>
    <div class="form-search delay03 text-center">

        <i class="ti-close" id="close-search"></i>
        <!-- <h3 class="text-center title-font">what are<br>your looking for ?</h3> -->
        <form method="get" action="{{url('/product/search')}}" role="search">
            <input type="text" class="form-control control-search des-font" value="" autocomplete="off"
                placeholder="Pencarian ..." aria-label="SEARCH" name="q">
            <button class="button_search title-font" type="submit">Cari</button>
        </form>
    </div>


    <!-- <script src="{{asset('assets')}}/js/jquery-3.3.1.min.js" defer=""></script> -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{asset('assets')}}/js/bootstrap.min.js" defer=""></script>
    <script src="{{asset('assets')}}/js/slick.min.js" defer=""></script>
    <script src="{{asset('assets')}}/js/function-main.js?v=2" defer=""></script>
    <script src="{{asset('assets')}}/js/function-imagezoom.js" defer=""></script>
    <script src="{{asset('assets')}}/js/custom" defer=""></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @stack('scripts')
</body>

</html>