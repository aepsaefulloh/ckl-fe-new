@extends('components.master')
@section('csrf_include')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')

<div class="banner margin_bottom_70">
    <div class="container">
        <h1 class="title-font title-banner">Keranjang</h1>
        <ul class="breadcrumb des-font">
            <li><a href="{{url('/')}}">Home</a></li>
            <li class="active">Keranjang</li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="table-responsive">
        <table class="table cart-table">
            <thead>
                <tr class="number-font">
                    <th class="product-thumbnail uppercase">Gambar</th>
                    <th class="product-name uppercase">Nama Produk</th>
                    <th class="product-price uppercase">Harga</th>
                    <th class="product-quantity uppercase">Kuantitas</th>
                    <th class="product-subtotal uppercase">Harga</th>
                    <th class="product-remove uppercase">#</th>
                </tr>
            </thead>
            <tbody>
                @forelse ((isset($_SESSION['CART']) && isset($_SESSION['CART']['DATA'])) ? $_SESSION['CART']['DATA'] :
                [] as $item)

                <tr class="item_cart">
                    <td class=" product-name">
                        <div class="product-img">
                            <img src="{{$item['IMAGE']}}" class="img-responsive" alt="">
                        </div>
                    </td>
                    <td class="product-desc">
                        <div class="product-info">
                            <a href="javascript:void(0)" class="title-font link-default">{{ $item['NAME'] }}</a>
                            <p class="number-font margin_top_20">SKU: <span class="menu-child-font">N/A</span>
                            </p>
                        </div>
                    </td>
                    <td>
                        <p class="price number-font">Rp. {{ number_format($item['PRICE'], 0, ',', '.') }}</p>
                    </td>
                    <td>
                        <p class="price number-font">{{$item['QTY']}}</p>
                    </td>
                    <td class="total-price">
                        <p class="price number-font">{{ number_format($item['PRICE'], 0, ',', '.').'x'.$item['QTY'] }}
                        </p>
                    </td>
                    <td class="product-remove">
                        <a href="#" class="btn-del link-default"><i class="ti-close"></i></a>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="6">
                        <p class="number-font">Your cart is empty</p>
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="table-cart-bottom margin_top_20 margin_bottom_50">
        <div class="row">
            <div class="col-md-7 col-sm-6 col-xs-12">
                <!-- <div class="cart-btn-group">
                            <a href="" class="btn-nixx number-font">Continue shopping</a>
                            <input type="submit" name="update" class="btn-nixx number-font" value="Update Cart">
                        </div> -->
                <!-- <div class="form-note">

                            <h3 class="title-font margin_bottom_20">Cart Note</h3>
                            <div class="form_coupon">
                                <div class="">
                                    <label for="CartSpecialInstructions" class="des-font margin_bottom_20">Special
                                        instructions for seller</label>
                                    <textarea rows="6" name="note" id="CartSpecialInstructions"
                                        class="form-control note--input des-font"></textarea>
                                </div>
                            </div>

                        </div> -->
            </div>
            <div class="col-md-5 col-sm-6 col-xs-12">
                <div class="cart-text">
                    <!-- <div class="cart-element flex">
                        <p class="des-font">Subtotal:</p>
                        <p class="number-font right">Rp. 169.000</p>
                    </div> -->

                    <div class="cart-element flex">
                        <p class="des-font">Total:</p>
                        <p class="number-font right">
                            Rp. @if (isset($_SESSION['CART']) && isset($_SESSION['CART']['COUNT_PRICE'])) {{ number_format($_SESSION['CART']['COUNT_PRICE'], 0, ',', '.') }} @else 0 @endif
                        </p>
                    </div>
                </div>
                <a href="/checkout" class="btn-nixx number-font">Checkout</a>
            </div>
        </div>
    </div>
</div>


@endsection