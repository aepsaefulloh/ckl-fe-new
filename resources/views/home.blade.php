@extends('components.master')

@section('content')

<div class="container relative">
    <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 info-slider-home1 text-left">
            @foreach($product_banner as $item)
            <div class="slider-nav">
                <div class="number-font text-left number-slider relative delay1_5">
                    0{{$loop->index+1}}
                    <figure class="line absolute">
                    </figure>
                </div>
                <h1 class="title-font title-slider delay1_5">{{ $item['PRODUCT'] }}</h1>
                <p class="des-font des-slider delay1_5">BlackBird collection of minimal, sleek and functional
                    Carryalls were <br class="hidden-xs">designed with creatives in mind.</p>
            </div>
            @endforeach
        </div>

        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 slider-home1 space_bot_70 absolute">
            @foreach($product_banner as $item)
            <div class="content-slider">
                <a href="{{ url('/product-detail'.'/'.$item['ID'].'/'.Helper::url_slug($item['PRODUCT'])) }}"><img src="{{$item['IMAGE']}}" class="img-responsive" alt=""></a>
            </div>
            @endforeach
        </div>

        <!--  -->


    </div>
</div>

<div class="collection-home6 container margin_bottom_130">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 clear-space-left space_right_10 content-collection">
            <a href="{{url('/')}}/product"><img src="https://landing.engotheme.com/html/nixx/demo/asset/img/collection1_home6.jpg"
                    class="img-responsive" alt=""></a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 space-left_10 space_right_10 relative flex content-collection">
            <a href="{{url('/')}}/product/category/crewneck-sweatshirt" class="inline-block over-hidden"><img
                    src="https://landing.engotheme.com/html/nixx/demo/asset/img/product3_home6.jpg"
                    class="img-responsive hover-zoom-out" alt=""></a>
            <a href="{{url('/')}}/product/category/crewneck-sweatshirt" class="absolute title-font link-collection link-default capital">Fashion Wanita</a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 space-left_10 space_right_10 relative flex content-collection">
            <a href="{{url('/')}}/product/category/tshirt" class="inline-block over-hidden"><img
                    src="https://landing.engotheme.com/html/nixx/demo/asset/img/product4_home6.jpg"
                    class="img-responsive hover-zoom-out" alt=""></a>
            <a href="{{url('/')}}/product/category/tshirt" class="absolute title-font link-collection link-default capital">Fashion Pria</a>
        </div>
        <div
            class="col-lg-3 col-md-3 col-sm-6 col-xs-12 clear-space-right space_left_10 relative flex content-collection">
            <a href="{{url('/')}}/product/category/long-sleeve" class="inline-block over-hidden"><img
                    src="https://landing.engotheme.com/html/nixx/demo/asset/img/product5_home6.jpg"
                    class="img-responsive hover-zoom-out" alt=""></a>
            <a href="{{url('/')}}/product/category/long-sleeve" class="absolute title-font link-collection link-default capital">shoes & sandal</a>
        </div>
    </div>
</div>

<div class="container margin_bottom_130 section-bestseller-home1 home6">
    <div class="row">
        <div class="col-md-12">
            <h1 class="title-font margin_bottom_30 title-bestseller">Produk Terbaru</h1>
            <div class="slick-newarrival">
                @forelse($product_new as $item)
                <div class="product">
                    <div class="img-product relative">
                        <a href="{{ url('/product-detail'.'/'.$item['ID'].'/'.Helper::url_slug($item['PRODUCT'])) }}"><img
                                src="{{$item['IMAGE']}}" class="img-responsive object-fit-300" alt=""></a>
                        <figure class="absolute uppercase label-new title-font text-center">new</figure>

                    </div>
                    <div class="info-product text-center">
                        <h4 class="des-font capital title-product space_top_20"><a href="#">{{ $item['PRODUCT'] }}</a>
                        </h4>
                        <p class="number-font price-product">
                            <span class="price">Rp.{{ number_format($item['PRICE'], 0, ',', '.') }}</span>
                        </p>
                    </div>
                </div>
                @empty
                <h5 class="text-center">Data Masih Kosong!</h5>
                @endforelse

            </div>

        </div>
    </div>
</div>

<div class="container shipping-home4 margin_bottom_150">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 content">
            <div class="flex">
                <img src="{{asset('assets')}}/img/offer.png" width="80" class="img-responsive" alt=""><span
                    class="title-font title-ship space_left_30">Discount</span>
            </div>
            <p class="des-font des-ship">Minimal Pembelian 3 Pcs Sudah dapat Potongan Rp. 5000/pcs</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 content">
            <div class="flex">
                <img src="{{asset('assets')}}/img/members.png" width="80" class="img-responsive" alt=""><span
                    class="title-font title-ship space_left_30">Member VVIP</span>
            </div>
            <p class="des-font des-ship">Mendapat Potongan Rp. 20.000/pcs dengan Minimal Pembelian 250 pcs cukup
                kirim Nama + ID Member. </p>
            <p><a href="tel:08112110082">Daftar Member</a></p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 content">
            <div class="flex">
                <img src="{{asset('assets')}}/img/badge.png" width="80" class="img-responsive" alt=""><span
                    class="title-font title-ship space_left_30">Quality</span>
            </div>
            <p class="des-font des-ship">Ready Stock Premium Lokal Brand</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 content margin_top_50">
            <div class="flex">
                <img src="{{asset('assets')}}/img/benefit.png" width="80" class="img-responsive" alt=""><span
                    class="title-font title-ship space_left_30">Benefit</span>
            </div>
            <p class="des-font des-ship">Ada banyak keuntungan yang didapat setelah bergabung sebagai Member CKL
            </p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 content margin_top_50">
            <div class="flex">
                <img src="{{asset('assets')}}/img/fast-delivery.png" width="80" class="img-responsive" alt=""><span
                    class="title-font title-ship space_left_30">Shipping</span>
            </div>
            <p class="des-font des-ship">Dalam situasi Pandemi COVID-19 pengiriman kami membutuhkan ±7 hari
                sejak masa pengemasan - pengiriman</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 content margin_top_50">
            <div class="flex">
                <img src="{{asset('assets')}}/img/customer-service.png" width="80" class="img-responsive" alt=""><span
                    class="title-font title-ship space_left_30">Service</span>
            </div>
            <p class="des-font des-ship">Kami berupaya memberikan pelayanan terbaik dan fast respon untuk
                kemudahan berbelanja secara Online</p>
        </div>
    </div>
</div>

<div class="container margin_bottom_100 section-bestseller-home1">
    <div class="row">
        <div class="col-md-12">
            <h1 class="title-font margin_bottom_30 title-bestseller">Produk Terlaris</h1>
            <div class="slick-bestseller-home7">
                @forelse($product_popular as $item)
                <div class="product margin_bottom_40">
                    <div class="img-product relative">
                        <a href="{{ url('/product-detail'.'/'.$item['ID'].'/'.Helper::url_slug($item['PRODUCT'])) }}"><img
                                src="{{$item['IMAGE']}}" class="img-responsive object-fit-300" alt=""></a>
                        
                        <!-- <figure class="absolute uppercase label-new title-font text-center">
                            New
                        </figure> -->

                    </div>
                    <div class="info-product text-center">
                        <h4 class="des-font capital title-product space_top_20"><a href="#">{{$item['PRODUCT']}}</a>
                        </h4>
                        <p class="number-font price-product">
                            <span class="price">Rp.{{ number_format($item['PRICE'], 0, ',', '.') }}</span>
                        </p>
                    </div>
                </div>
                @empty
                <h5 class="text-center">Data Masih Kosong!</h5>
                @endforelse
            </div>
        </div>
    </div>
</div>

<div class="container brand-home4 margin_bottom_70">
    @forelse($brand as $item)

    <a href="javascript:void(0)" class="inline-block margin_bottom_80">
        <img src="{{$item['IMAGE']}}" class="img-fluid" alt="">
    </a>

    @empty
    <h5 class="text-center">Data Masih Kosong!</h5>
    @endforelse


</div>

<div class="newsletter-home3 space_bot_150 space_top_130 BG">
    <h1 class="title-font capital title-newsletter text-center">Subscribe</h1>
    <p class="des-font des-newsletter space_bot_60 text-center">Ikuti untuk mendapatkan potongan harga</p>
    <form class="form-group des-font flex" method="post" target="_blank">
        <input type="email" name="EMAIL" class="form-control" placeholder="Alamat Email">
        <button type="submit" class="menu-font uppercase">subscribe</button>
    </form>
</div>

<div class="container blog-home4 space_top_bot_150">
    <div class="row">
        <h1 class="title-blog title-font text-center">Artikel</h1>
        <p class="des-font des-blog text-center space_bot_60">I did not even know that there were any better
            conditions to escape to, but I was more than willing to<br>take my chances among people fashioned
            after.</p>
        @forelse ($content as $item)
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 content-blog space_right_10 space_left_10 clear-none">
            <a href="{{ url('/content-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}"
                class="inline-block over-hidden">
                <img src="{{$item['IMAGE']}}" class="img-responsive hover-zoom-out object-fit-300" alt="">
            </a>
            <h2 class="title-font capital title-post"><a
                    href="{{ url('/content-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}">{{$item['TITLE']}}</a>
            </h2>
            <p class="des-font day-post">{{ Date('d-M-Y', strtotime($item['CREATE_TIMESTAMP'])) }}</p>
            <p class="des-font des-post">{{ $item['TITLE'] }}</p>
        </div>
        @empty
        <h5 class="text-center">Data Masih Kosong!</h5>
        @endforelse
    </div>
</div>
@endsection