<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;



class PaymentController extends Controller
{
    public function __construct() {
        session_start();
    }
    
    public function index(){
        // Data Province
        $prov = $this->http_get($this->url_api().'province');
        
        if ($prov && $prov['status'] == 200) {
            $this->data['province'] = $prov['data'];
        } else {
            $this->data['province'] = [];
        }
        
        return view('components.payment.profile', $this->data);
    }

    public function checkout_store(Request $request) {
        if (isset($_SESSION['CART']) && isset($_SESSION['CART']['DATA']) && count($_SESSION['CART']['DATA']) > 0) {
            $data = $request->all();
            $data["product"] = $_SESSION['CART']['DATA'];

            $http_post = $this->http_post($this->url_api().'checkout-store', $data);

            if ($http_post && $http_post['status'] == 200 && count($http_post['data']) > 0) {
                unset($_SESSION['CART']);
                
                $_SESSION['CHECKOUT_SUCCESS'] = [
                    "TIME"          => time(),
                    "NAME"         => $http_post['data']['NAME'],
                    "NO_TR"        => $http_post['data']['NO_TR'],
                    "PRODUCT"      => $http_post['data']['PRODUCT'],
                    "COUNT_PRICE"  => $http_post['data']['COUNT_PRICE']
                ];
            }
 
            return redirect()->route('order-success');
        } else {
            return redirect()->back();
        }
    }

    public function add_cart(Request $request){
        if (isset($_SESSION['CART']) && isset($_SESSION['CART']['DATA']) && count($_SESSION['CART']['DATA']) > 0) {
            $cek_session = collect($_SESSION['CART']['DATA'])->where('CODE', '=', $request->code)->where('SIZE', '=', $request->size)->values();


            if (isset($cek_session[0]) && $cek_session[0]['CODE'] == $request->code && $cek_session[0]['SIZE'] == $request->size) {
               foreach ($_SESSION['CART']['DATA'] as $key => $value) {
                   $_SESSION['CART']['DATA'][$key]['QTY'] = $value['QTY'] + (int) $request->qty;
                   $_SESSION['CART']['DATA'][$key]['COUNT_PRICE'] = (int) $value['PRICE'] * ($value['QTY'] + (int) $request->qty);
               }
            } else {
                $data = [
                    "CODE"          => $request->code,
                    "IMAGE"         => $request->image,
                    "NAME"          => $request->name,
                    "PRICE"         => $request->price,
                    "COUNT_PRICE"   => (int) $request->price * (int) $request->qty,
                    "SIZE"          => $request->size,
                    "QTY"           => $request->qty
                ];

                array_push($_SESSION['CART']['DATA'], $data);
                array_multisort($_SESSION['CART']['DATA']);
            } 
            $_SESSION['CART']['COUNT_PRICE'] = collect($_SESSION['CART']['DATA'])->sum('COUNT_PRICE');
            $_SESSION['CART']['COUNT_QTY'] = collect($_SESSION['CART']['DATA'])->sum('QTY');
        } else {
            $data = [
                "CODE"          => $request->code,
                "IMAGE"         => $request->image,
                "NAME"          => $request->name,
                "PRICE"         => $request->price,
                "COUNT_PRICE"   => (int) $request->price * (int) $request->qty,
                "SIZE"          => $request->size,
                "QTY"           => $request->qty
            ];

            $_SESSION['CART']['DATA'] = array($data);
            $_SESSION['CART']['COUNT_PRICE'] = $data['COUNT_PRICE'];
            $_SESSION['CART']['COUNT_QTY'] = $data['QTY'];
        }

        return response()->json($_SESSION['CART']['COUNT_QTY']);
    }

    public function remove_cart() {
        if (isset($_SESSION) && isset($_SESSION['CART'])) {
            unset($_SESSION['CART']);
        }

        return redirect()->back();
    }
    public function cart(){
        return view('components.payment.cart');
    }


    public function orderSuccess(){
        if (isset($_SESSION['CHECKOUT_SUCCESS'])) {
            if (time() - $_SESSION['CHECKOUT_SUCCESS']['TIME'] > (60*10)) {
                unset($_SESSION['CHECKOUT_SUCCESS']);
                return redirect()->route('store');
            } else {
                return view('components.payment.order-success');
            }
        } else {
            return redirect()->route('store');
        }
    }
}
