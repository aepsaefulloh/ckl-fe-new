<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct() {
        session_start();
    }
    
    public function index(){
        // Data Product Banner
        $product_banner = $this->http_get($this->url_api().'product?perpage=6');
        // dd($product_banner['data']);
        if ($product_banner && $product_banner['status'] == 200) {
            $this->data['product_banner'] = $product_banner['data'];
        } else {
            $this->data['product_banner'] = [];
        }

        // Data Product
        $product = $this->http_get($this->url_api().'product?page=1&perpage=6');
        if ($product && $product['status'] == 200) {
            $this->data['product'] = $product['data'];
        } else {
            $this->data['product'] = [];
        }

        // Data New Product
        $product_new = $this->http_get($this->url_api().'product?page=1&perpage=8');
        if ($product_new && $product_new['status'] == 200) {
            $this->data['product_new'] = $product_new['data'];
        } else {
            $this->data['product_new'] = [];
        }

         // Data New Product
         $product_popular = $this->http_get($this->url_api().'product?page=1&perpage=16');
            if ($product_popular && $product_popular['status'] == 200) {
             $this->data['product_popular'] = $product_popular['data'];
         } else {
             $this->data['product_popular'] = [];
         }

        // Data Content
        $content = $this->http_get($this->url_api().'content?category=5&perpage=5');
        if ($content && $content['status'] == 200) {
            $this->data['content'] = $content['data'];
        } else {
            $this->data['content'] = [];
        }

        // Data Brand
        $brand = $this->http_get($this->url_api().'content?category=12');
        if ($brand && $brand['status'] == 200) {
            $this->data['brand'] = $brand['data'];
        } else {
            $this->data['brand'] = [];
        }
        
        return view('home', $this->data);
    }

 

  
}