@extends('components.master')

@section('content')
<div class="banner margin_bottom_150">
    <div class="container">
        <h1 class="title-font title-banner">Contact</h1>
        <ul class="breadcrumb des-font">
            <li><a href="{{url('/')}}">Home</a></li>
            <li class="active">Contact us</li>
        </ul>
    </div>
</div>

<div class="content-contact margin_bottom_150 container">
    <div class="row">
        <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 local">
            <h1 class="title-local title-font margin_bottom_30">Find us here</h1>
            <div class="address">
                <i class="ti-location-pin"></i>
                <span class="menu-font bold capital">address:</span>
                <p class="des-font text">1234 Heaven Stress, Beverly hill, Melbourne, USA.</p>
            </div>
            <!--  -->
            <div class="email">
                <i class="ti-email"></i>
                <span class="menu-font bold capital">email:</span>
                <p class="des-font text">Contact@example.com</p>
            </div>
            <!--  -->
            <div class="phone">
                <i class="ti-headphone"></i>
                <span class="menu-font bold capital">number phone:</span>
                <p class="des-font text">(008) 123 456 789</p>
                <p class="des-font text">(008) 987 654 321</p>
            </div>
            <p class="des-local des-font margin_top_50">I did not even know that there were any better
                conditions to escape to, but I was more
                than willing to take my chances among people fashioned after. I did not even know that
                there were any better conditions to escape to, but I was more than willing to take my
                chances among people fashioned after.</p>
        </div>
        <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 subscribe">
            <h1 class="title-font title-sub margin_bottom_30">Contact us</h1>
            <form class="form-group des-font" method="post" target="_blank">
                <input type="text" name="NAME" class="form-control margin_bottom_30" placeholder="Your name (required)">
                <input type="email" name="EMAIL" class="form-control margin_bottom_30"
                    placeholder="Your email (required)">
                <input type="text" name="SUBJECT" class="form-control margin_bottom_30" placeholder="Subject">
                <input type="text" name="NAME" class="form-control margin_bottom_30" placeholder="Your message">
                <button type="submit" class="full-width uppercase menu-font">subscribe</button>
            </form>
        </div>
    </div>
</div>

<div class="relative over-hidden container margin_bottom_150 map-contact">
    <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.2378219899956!2d106.85416311455499!3d-6.232347895488605!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f458b645ef25%3A0x33840d0b815151a!2sSketsahouse!5e0!3m2!1sen!2sid!4v1652072072768!5m2!1sen!2sid"
        width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"
        referrerpolicy="no-referrer-when-downgrade"></iframe>
</div>
@endsection