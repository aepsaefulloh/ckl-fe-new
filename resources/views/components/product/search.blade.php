@extends('components.master')

@section('content')
<div class="banner margin_bottom_70">
    <div class="container">
        <h1 class="title-font title-banner">Produk</h1>
        <ul class="breadcrumb des-font">
            <li><a href="{{url('/')}}">Beranda</a></li>
            <li class="active">Produk</li>
        </ul>
    </div>
</div>
<!--  -->
<div class="container shop-page margin_bottom_70">
    <!-- <div class="row">
        <div class="col-12">
            <div class="container margin_bottom_130 section-bestseller-home1 home6">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="title-font margin_bottom_10 title-bestseller text-center">Diskon</h1>
                        <p class="des-font margin_bottom_50 des-bestseller text-center">I did not even know that there
                            were any
                            better conditions to escape to, but I was more than willing
                            to take my chances among people fashioned after.</p>
                        <div class="slick-newarrival">
                            @forelse($product as $item)
                            <div class="product">
                                <div class="img-product relative">
                                    <a href="#"><img src="{{$item['IMAGE']}}" class="img-responsive" alt=""></a>
                                    <figure class="absolute uppercase label-new title-font text-center">new</figure>
                                    <div class="product-icon text-center absolute">
                                        <form method="post" action="/cart/add" enctype="multipart/form-data"
                                            class="inline-block icon-addcart">
                                            <input type="hidden" name="id" value="" />
                                            <button type="submit" name="add" class="enj-add-to-cart-btn btn-default"><i
                                                    class="ti-bag"></i></button>
                                        </form>
                                        <a href="#" class="icon-heart inline-block"><i class="ti-heart"></i></a>
                                        <a href="#" class="engoj_btn_quickview icon-quickview inline-block"
                                            title="quickview">
                                            <i class="ti-more-alt"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="info-product text-center">
                                    <h4 class="des-font capital title-product space_top_20"><a
                                            href="#">{{ $item['PRODUCT'] }}</a>
                                    </h4>
                                    <p class="number-font price-product">
                                        <span class="price">Rp.{{ number_format($item['PRICE'], 0, ',', '.') }}</span>
                                    </p>
                                </div>
                            </div>
                            @empty
                            <h5 class="text-center">Data Masih Kosong!</h5>
                            @endforelse

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 sidebar-left">

            <ul class="category margin_bottom_70">
                <li>
                    <h1 class="title-font title">Kategori</h1>
                </li>
                @foreach ($category as $item)
                <li><a href="{{url('product/category/'.strtolower(str_replace(' ', '-', trim($item['CATEGORY']))))}}" class="des-font link-collection">{{$item['CATEGORY']}}</a></li>
                @endforeach
            </ul>

        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 content-shop">
            <div class="row btn-function-shop">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 margin_bottom_50">
                    <span class="des-font showing hidden-xs">Showing 1–9 of 50 results</span>
                    <button class="active" id="btn-grid"><i class="ti-layout-grid3-alt"></i></button>
                    <button id="btn-list"><i class="ti-list"></i></button>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 margin_bottom_50 text-right select-view">
                    <button><i class="ti-eye"></i></button>
                    <select id="select-show">
                        <option>Sort by popularity</option>
                        <option>Featured</option>
                        <option>Best selling</option>
                        <option>Alphabetically, A - Z</option>
                        <option>Price, hight to low</option>
                        <option>Price, low to hight</option>
                    </select>
                </div>
            </div>
            <div class="row">
                @forelse($product as $item)
                <div class="product col-lg-4 col-md-4 col-sm-6 col-xs-6 margin_bottom_50">
                    <div class="img-product relative">
                        <a href="{{ url('/product-detail'.'/'.$item['ID'].'/'.Helper::url_slug($item['PRODUCT'])) }}">
                            <img src="{{ $item['IMAGE'] }}" class="img-responsive object-fit-300" alt=""></a>
                        <!-- <figure class="absolute uppercase label-new title-font text-center">new</figure> -->
                        <!-- <div class="product-icon text-center absolute">
                            <form method="post" action="/cart/add" enctype="multipart/form-data"
                                class="inline-block icon-addcart">
                                <input type="hidden" name="id" value="" />
                                <button type="submit" name="add" class="enj-add-to-cart-btn btn-default"><i
                                        class="ti-bag"></i></button>
                            </form>
                            <a href="#" class="icon-heart inline-block"><i class="ti-heart"></i></a>
                            <a href="#" class="engoj_btn_quickview icon-quickview inline-block" title="quickview">
                                <i class="ti-more-alt"></i>
                            </a>
                        </div> -->
                    </div>
                    <div class="product-info">
                        <div class="info-product text-center">
                            <h4 class="des-font capital title-product space_top_20"><a href="#">{{$item['PRODUCT']}}</a>
                            </h4>
                            <p class="number-font price-product"><span class="price">Rp.
                                    {{ number_format($item['PRICE'], 0, ',', '.') }}</span></p>
                            <p class="des-font des-product">Tote bag from Mansur Gavriel in Saddle. Calf
                                leather. Open compartment. Interior side pocket. Top
                                handles. Detachable, adjustable shoulder strap. Goldtone hardware. Embossed logo
                                detailing at exterior.
                                Tonal patent coated interior.</p>
                        </div>
                        <!-- <div class="btn-product-list">
                            <form method="post" action="/cart/add" enctype="multipart/form-data"
                                class="inline-block icon-addcart">
                                <input type="hidden" name="id" value="" />
                                <button type="submit" name="add"
                                    class="enj-add-to-cart-btn btn-default des-font uppercase">add to
                                    cart</button>
                            </form>
                            <a href="#" class="icon-heart inline-block"><i class="ti-heart"></i></a>
                            <a href="#" class="engoj_btn_quickview icon-quickview inline-block" title="quickview">
                                <i class="ti-more-alt"></i>
                            </a>
                        </div> -->
                    </div>
                </div>
                @empty
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h5 class="text-center">Product Masih Kosong!</h5>
                </div>
                @endforelse
                <!--  -->
            </div>
            <!--  -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="pagination">
                        <li
                            class="@if (!isset($_GET['page']) || (isset($_GET['page']) && $_GET['page'] == 1)) disabled @endif">
                            <a href="{{ (isset($_GET['page'])) ? url('product?page='.($_GET['page']-1)) : url('product?page=1') }}"
                                class="des-font border "><i class="ti-arrow-left"></i></a>
                        </li>
                        @for ($i = 1; $i < $paginate_all; $i++) <li
                            class="@if ((!isset($_GET['page']) && 1 == $i) ||(isset($_GET['page']) && $_GET['page'] == $i)) disabled active @endif">
                            <a href="{{ url('product?page=').$i }}" class="des-font active">{{ $i }}</a></li>
                            @endfor
                            <li
                                class="@if ((!isset($_GET['page']) && 1 == $paginate_all) || (isset($_GET['page']) && $_GET['page'] == $paginate_all)) disabled @endif">
                                <a href="{{ (isset($_GET['page'])) ? url('product?page='.($_GET['page']+1)) : url('product?page=2') }}"
                                    class="des-font border"><i class="ti-arrow-right"></i></a>
                            </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection