@extends('components.master')

@section('content')

<div class="banner margin_bottom_70">
    <div class="container">
        <h1 class="title-font title-banner banner-product-detail">{{ $product['PRODUCT'] }}</h1>
        <ul class="breadcrumb des-font">
            <li><a href="{{url('/')}}">Beranda</a></li>
            <li><a href="{{url('/product')}}">Produk</a></li>
            <li class="active">{{ $product['PRODUCT'] }}</li>
        </ul>
    </div>
</div>


<div class="container product-detail margin_bottom_70">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 margin_bottom_50">
            <div class="slick-product-detail margin_bottom_20">

                <div>
                    <img src="{{ $product['IMAGE'][0]['NAME'] }}" id="myImage"
                        class="img-responsive full-width object-fit-500" alt="">
                </div>
            </div>
            <div class="slick-nav-product-detail">
                @foreach ($product['IMAGE'] as $item)
                <div>
                    <img src="{{ $item['NAME'] }}" class="img-responsive object-fit-300" alt="">
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 info-product-detail">
            <h1 class="title-font title-product margin_bottom_30">{{ $product['PRODUCT'] }}</h1>
            <p class="number-font price margin_bottom_40">Rp. {{ number_format($product['PRICE'], 0, ',', '.') }}</p>
            <p class="product-preview margin_bottom_50">
                <i class="ti-star"></i>
                <i class="ti-star"></i>
                <i class="ti-star"></i>
                <i class="ti-star"></i>
                <i class="ti-star"></i>
                <span class="relative line-space">__</span>
                <span class="des-font">(02) Reviews</span>
            </p>
            <div class="margin_bottom_30">
                <select class="menu-font">
                    <option class="uppercase">Please select a color</option>
                    <option class="capital">Black</option>
                    <option class="capital">red</option>
                    <option class="capital">white</option>
                </select>
            </div>
            <div class="flex margin_bottom_50 border-bot space_bot_50 btn-function">
                <div class="input-number-group">
                    <div class="relative input-number-custom">
                        <div class="input-group-button absolute down-btn">
                            <span class="input-number-decrement ti-angle-down"></span>
                        </div>
                        <input id="qty" name="qty" class="input-number menu-font" type="number" min="0" max="1000"
                            value="1">
                        <div class="input-group-button absolute up-btn">
                            <span class="input-number-increment ti-angle-up"></span>
                        </div>
                    </div>
                </div>
                <form class="inline-block icon-addcart">
                    <button id="btn_add_cart" type="button" name="add"
                        class="enj-add-to-cart-btn btn-default menu-font uppercase">add to
                        cart</button>
                </form>
                <a href="#" class="icon-heart"><i class="ti-heart"></i></a>
                <a href="#" class="engoj_btn_quickview icon-quickview" title="quickview">
                    <i class="ti-more-alt"></i>
                </a>
            </div>
            <div class="inline-block border-bot">
                <div class="inline-block margin_bottom_50">
                    <button class="accordion menu-font btn-tab">Description</button>
                    <div class="panel">
                        <p class="des-font des-tab">{!! $product['SPECS'] !!}
                        </p>

                    </div>
                </div>

                <div class="inline-block margin_bottom_50">
                    <button class="accordion menu-font btn-tab">Customer reviews (0)</button>
                    <div class="panel">
                        <div class="review margin_bottom_50">
                            <h1 class="menu-font title-review">Reviews</h1>
                            <p class="des-font content-review">There are no review yet.</p>
                        </div>
                        <div class="form-review">
                            <h1 class="menu-font title-form">Be the first to review “Embossed blackpack in
                                brown” </h1>
                            <p class="des-font des-review margin_bottom_50">Your email address will not be
                                published. Required fields are marked *</p>
                            <form>
                                <input type="text" name="name" placeholder="First name*"
                                    class="margin_bottom_20 des-font">
                                <input type="email" name="EMAIL" placeholder="Email*" class="margin_bottom_20 des-font">
                                <textarea placeholder="Message*" class="margin_bottom_20 des-font"></textarea>
                                <p class="menu-font margin_bottom_20 rating">Your rating
                                    <i class="ti-star"></i>
                                    <i class="ti-star"></i>
                                    <i class="ti-star"></i>
                                    <i class="ti-star"></i>
                                    <i class="ti-star"></i>
                                </p>
                                <button type="submit" class="menu-font uppercase">submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-more">
                <p class="des-font margin_bottom_30 margin_top_50"><span class="menu-font">SKU:</span> N/A</p>
                <p class="margin_bottom_30">
                    <span class="menu-font margin_right_10">Categories:</span>
                    <a href="#" class="delay03 margin_right_10">Bag,</a>
                    <a href="#" class="delay03 margin_right_10">Women,</a>
                    <a href="#" class="delay03 margin_right_10">New Arrival</a>
                </p>
                <p class="margin_bottom_30">
                    <span class="menu-font margin_right_30">Share:</span>
                    <a href="#" class="delay03 margin_right_30"><i class="ti-facebook"></i></a>
                    <a href="#" class="delay03 margin_right_30"><i class="ti-twitter-alt"></i></a>
                    <a href="#" class="delay03 margin_right_30"><i class="ti-pinterest"></i></a>
                    <a href="#" class="delay03 margin_right_30"><i class="ti-linkedin"></i></a>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="container margin_bottom_130 section-bestseller-home1">
    <div class="row">
        <div class="col-md-12">
            <h1 class="title-font margin_bottom_10 title-bestseller">Related products</h1>
            <p class="des-font margin_bottom_50 des-bestseller">I did not even know that there were any better
                conditions to escape to, but I was more than willing
                to take my chances among people fashioned after.</p>
            <div class="slick-bestseller">

                @forelse($productRelated as $item)
                <div class="product">
                    <div class="img-product relative">
                        <a href="#"><img src="{{$item['IMAGE']}}" class="img-responsive object-fit-300" alt=""></a>
                        <!-- <figure class="absolute uppercase label-new title-font text-center">new</figure> -->
                        <!-- <div class="product-icon text-center absolute">
                            <form method="post" action="/cart/add" enctype="multipart/form-data"
                                class="inline-block icon-addcart">
                                <input type="hidden" name="id" value="" />
                                <button type="submit" name="add" class="enj-add-to-cart-btn btn-default"><i
                                        class="ti-bag"></i></button>
                            </form>
                            <a href="#" class="icon-heart inline-block"><i class="ti-heart"></i></a>
                            <a href="#" class="engoj_btn_quickview icon-quickview inline-block" title="quickview">
                                <i class="ti-more-alt"></i>
                            </a>
                        </div> -->
                    </div>
                    <div class="info-product text-center">
                        <h4 class="des-font capital title-product space_top_20"><a
                                href="#">{{ $product['PRODUCT'] }}</a></h4>
                        <p class="number-font price-product"><span class="price">Rp.
                                {{ number_format($product['PRICE'], 0, ',', '.') }}</span></p>
                    </div>
                </div>

                @empty
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h5 class="text-center">Product Masih Kosong!</h5>
                </div>
                @endforelse
            </div>

        </div>
    </div>
</div>



@endsection
@push('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
$("input[name=size]:radio").click(function() {
    if ($('input[name=size]:checked').val() && "<?php echo $product['AVAIL']; ?>" == 1) {
        $('#btn_add_cart').removeClass('disabled');
    } else if ($('input[name=size]:checked').val() && "<?php echo $product['AVAIL']; ?>" == 0) {
        $('#btn_add_cart').addClass('disabled').html('SOLD OUT');
    } else {
        $('#btn_add_cart').addClass('disabled');
    }
});

// Onchange QTY
$("#qty").on('mouseup change click', function() {
    var min = parseInt($(this).attr('min'));

    if ($(this).val() < min) {
        $(this).val(min);
        alert('QTY Minimal ' + min)
    }
});

// Button Add Cart
$('#btn_add_cart').on('click', function() {
    // Result Data Form
    let img_product = "{{ $product['IMAGE'][0]['NAME'] }}";
    let code_product = "{{ $product['CODE'] }}";
    let name_product = "{{ $product['PRODUCT'] }}";
    let size_product = $('input[name=size]:checked').val();
    let qty_product = $('#qty').val();
    let price_product = "{{ $product['PRICE'] }}";

    $.ajax({
        url: "{{ route('add_cart') }}",
        type: "POST",
        data: {
            "_token": "{{ csrf_token() }}",
            "image": img_product,
            "code": code_product,
            "name": name_product,
            "size": size_product,
            "qty": qty_product,
            "price": price_product
        },
        success: function(response) {
            // Push Count Cart to badge
            $('#count_cart').html(response);
            // Sweetalert js
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: "Product success add to cart",
                showConfirmButton: false,
                timer: 1500
            })
            window.location.reload();
        }
    })
})
</script>
@endpush