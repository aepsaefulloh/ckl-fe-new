@extends('components.master')

@section('content')

<div class="banner margin_bottom_70">
    <div class="container">
        <h1 class="title-font title-banner">Article</h1>
        <ul class="breadcrumb des-font">
            <li><a href="{{url('/')}}">Home</a></li>
            <li class="active">Article</li>
        </ul>
    </div>
</div>

<div class="container blog-page margin_bottom_70">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content-blog">
            <div class="row">
                @forelse($content as $item)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 space_right_10 margin_bottom_70">
                    <a href="{{ url('/content-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}"
                        class="inline-block over-hidden">
                        <img src="{{ $item['IMAGE'] }}" class="img-responsive hover-zoom-out object-fit-300" alt=""></a>
                    <h2 class="title-font capital title-post"><a href="{{ url('/content-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}">{{$item['TITLE']}}</a></h2>
                    <p class="des-font day-post">{{ Date('d-M-Y', strtotime($item['CREATE_TIMESTAMP'])) }}</p>
                    <p class="des-font des-post">{{$item['SUMMARY']}}</p>
                </div>
                @empty
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p>No data</p>
                </div>
                @endforelse

            </div>
            <!--  -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="pagination">
                        <li
                            class="@if (!isset($_GET['page']) || (isset($_GET['page']) && $_GET['page'] == 1)) disabled @endif">
                            <a href="{{ (isset($_GET['page'])) ? url('content?page='.($_GET['page']-1)) : url('content?page=1') }}"
                                class="des-font border "><i class="ti-arrow-left"></i></a>
                        </li>
                        @for ($i = 1; $i < $paginate_all; $i++) <li
                            class="@if ((!isset($_GET['page']) && 1 == $i) ||(isset($_GET['page']) && $_GET['page'] == $i)) disabled active @endif">
                            <a href="{{ url('content?page=').$i }}" class="des-font active">{{ $i }}</a></li>
                            @endfor
                            <li
                                class="@if ((!isset($_GET['page']) && 1 == $paginate_all) || (isset($_GET['page']) && $_GET['page'] == $paginate_all)) disabled @endif">
                                <a href="{{ (isset($_GET['page'])) ? url('content?page='.($_GET['page']+1)) : url('content?page=2') }}"
                                    class="des-font border"><i class="ti-arrow-right"></i></a>
                            </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection