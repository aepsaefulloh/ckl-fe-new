@extends('components.master')

@section('content')

<div class="banner margin_bottom_70">
    <div class="container">
        <h1 class="title-font title-banner">Artikel</h1>
        <ul class="breadcrumb des-font">
            <li><a href="{{url('/')}}">Beranda</a></li>
            <li><a href="{{url('/content')}}">Artikel</a></li>
            <li class="active">{{ $content['TITLE'] }}</li>
        </ul>
    </div>
</div>
<!--  -->
<div class="container blog-page margin_bottom_70 blog-detail-page">
    <div class="row flex">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 content-blog-detail">
            <a href="#" class="inline-block over-hidden"><img src="{{$content['IMAGE']}}"
                    class="full-width hover-zoom-out" alt=""></a>
            <h1 class="title-font title-post">{{ $content['TITLE'] }} </h1>
            <p class="day-post des-font">{{ Date('Y-m-d', strtotime($content['CREATE_TIMESTAMP'])) }}</p>
            {!! $content['CONTENT'] !!}
            <div class="collection-post space_top_bot_20 border-bot">
                <i class="ti-folder space_right_10"></i>
                <span class="menu-font">Categories:</span>
                <a href="#" class="des-font"> {{ $content['CATEGORY'] }} </a>
            </div>
            <div class="view-post space_top_bot_20 margin_bottom_150">
                <span class="des-font space_right_30"><i class="ti-folder space_right_10"></i>0</span>
                <span class="des-font space_right_30"><i class="ti-eye space_right_10"></i>907</span>
                <span class="des-font space_right_30"><i class="ti-heart space_right_10"></i>0</span>
            </div>
            <div class="comment-post">
                <h1 class="title-font title-comment-post">Leave a reply</h1>
                <p class="des-font des-comment-post margin_bottom_50">Your email address will not be published. Required
                    fields are marked *</p>
                <form class="form-group des-font" method="post">
                    <textarea type="text" name="comment" class="form-control" placeholder="Comment"></textarea>
                    <input type="text" name="name" class="form-control inline-block" placeholder="Name*">
                    <input type="email" name="email" class="form-control right inline-block" placeholder="Email*">
                    <input type="text" name="website" class="form-control" placeholder="Website*">
                    <button type="submit" class="menu-font uppercase">subscribe</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection