<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PromoController;
use App\Http\Controllers\PaymentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/product', [ProductController::class, 'index'])->name('product');
Route::get('/product/search/', [ProductController::class, 'product_search'])->name('product');
Route::get('/product/category/{name}', [ProductController::class, 'product_category'])->name('product');
Route::get('/product-detail/{id}/{title}', [ProductController::class, 'detail'])->name('product.detail');

Route::get('/content', [ContentController::class, 'index'])->name('content');
Route::get('/content-detail/{id}/{title}', [ContentController::class, 'detail'])->name('content.detail');

Route::get('/contact', [ContactController::class, 'index'])->name('contact');

Route::get('/promo', [PromoController::class, 'index'])->name('promo');

Route::post('/add-cart', [PaymentController::class, 'add_cart'])->name('add_cart');
Route::get('/remove-cart', [PaymentController::class, 'remove_cart'])->name('remove_cart');
Route::get('/cart', [PaymentController::class, 'cart'])->name('cart');

Route::get('/order-success', [PaymentController::class, 'orderSuccess'])->name('order-success');
